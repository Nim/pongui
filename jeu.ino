#include "Bonus.hpp"
#include "Game.hpp"
#include "LedStrip.hpp"
#include "Screensaver.hpp"
#include <FastLED.h>

#include "config.h"

Strip strip = Strip();
Screensaver screensaver = Screensaver();
Game game = Game();

void setup() {
    Serial.begin(57600);
    Serial.println(F("Hello, world!"));

    pinMode(BUTTON_FAR_PIN, INPUT_PULLUP);
    pinMode(BUTTON_NEAR_PIN, INPUT_PULLUP);

// Initialize global FastLED Controller, but do not set data array yet
#ifdef LED_CLOCK_PIN
    FastLED.addLeds<LED_TYPE, LED_PIN, LED_CLOCK_PIN, COLOR_ORDER>(NULL, 0);
#else
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(NULL, 0);
#endif
    FastLED.setBrightness(BRIGHTNESS);

    strip.init();
}

void loop() {
    FastLED.show();

    if(game.state != WAITING)
        game.play();
    else if(game.should_start())
        game.init();
    else
        screensaver.animate();
}
