#include "LedStrip.hpp"

void Strip::init() {
    // Turn strip black, without caring for offsets
    this->num_leds = NUM_LEDS;
    this->leds = (CRGB *)malloc(NUM_LEDS * sizeof(CRGB));
    FastLED[0].setLeds(this->leds, NUM_LEDS);
    turn_all_off();
    FastLED.show();
    free(this->leds);

    // Re-initialize strip with offsets
    this->leds = (CRGB *)malloc((NUM_LEDS - END_OFFSET) * sizeof(CRGB));
    this->num_leds = EFFECTIVE_NUM_LEDS;
    FastLED[0].setLeds(this->leds, NUM_LEDS - END_OFFSET);
    this->leds = &(this->leds[START_OFFSET]);
};

void Strip::turn_on(CRGB color, short position) {
#ifdef DEBUG
    if(position < 0 || position > num_leds - 1) {
        Serial.print(F("Turn on out of range: "));
        Serial.println(position);
        return;
    }
#endif
    leds[position] = color;
}

void Strip::turn_off(short position) {
    turn_on(CRGB::Black, position);
}

void Strip::turn_range_on(CRGB color, short start, short end, byte step = 1) {
#ifdef DEBUG
    if(start > end || start < 0 || end > num_leds - 1) {
        Serial.print(F("Turn range on out of range: "));
        Serial.print(start);
        Serial.print(" ");
        Serial.print(end);
        Serial.print(" ");
        Serial.println(step);
        return;
    }
#endif
    for(uint16_t i = start; i <= end; i += step)
        turn_on(color, i);
}

void Strip::turn_range_rainbow(short start, short end, uint8_t initialhue = 0, uint8_t deltahue = 5) {
#ifdef DEBUG
    if(start > end || start < 0 || end > num_leds - 1) {
        Serial.print(F("Rainbow out of range: "));
        Serial.print(start);
        Serial.print(" ");
        Serial.println(end);
        return;
    }
#endif
    CHSV hsv;
    hsv.hue = initialhue;
    hsv.val = 255;
    hsv.sat = 240;
    for(uint16_t i = start; i <= end; i += 1) {
        turn_on(hsv, i);
        hsv.hue += deltahue;
    }
}

void Strip::turn_range_off(short start, short end, byte step = 1) {
    turn_range_on(CRGB::Black, start, end, step);
}

void Strip::turn_all_off() {
    memset8((void *)leds, 0, sizeof(struct CRGB) * num_leds);
}

void Strip::set_random_color_led(short position) {
    turn_on(get_random_color(), position);
}

CRGB Strip::get_random_color() {
    return CRGB(random(256), random(256), random(256));
}

void Strip::set_animation(Animation *anim) {
    if(animation && animation->can_be_deleted)
        delete animation;
    animation = anim;
    animation->init();
}

bool Strip::animate() {
    if(animation) {
        if(animation->done()) {
            if(animation->can_be_deleted)
                delete animation;
            animation = nullptr;
        } else {
            animation->play();
        }
    }
    return animation != nullptr;
}
