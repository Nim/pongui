#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include "Animation.hpp"
#include "Player.hpp"
#include "config.h"
#include <Arduino.h>
#include <FastLED.h>

class Animation;

class Strip {
  private:
    Animation *animation;
    CRGB *leds;

  public:
    uint16_t num_leds;
    void init();
    void turn_on(CRGB color, short position);
    void turn_off(short position);
    void turn_range_on(CRGB color, short start, short end, byte step = 1);
    void turn_range_rainbow(short start, short end, uint8_t initialhue = 0, uint8_t deltahue = 5);
    void turn_range_off(short start, short end, byte step = 1);
    void turn_all_on(CRGB color);
    void turn_all_off();
    CRGB get_random_color();
    void set_random_color_all();
    void set_random_color_led(short position);
    void set_animation(Animation *animation);
    bool animate();
};

#endif
