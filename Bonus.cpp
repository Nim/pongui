#include "Bonus.hpp"
#include "jeu.hpp"

Bonus::Bonus(short len, uint32_t probability, CRGB color) {
    this->len = len;
    this->probability = probability;
    this->color = color;
}

void Bonus::action_on_display() {
    state = DISPLAYED;
}

void Bonus::action_on_take(Ball *ball) {
    if(ball->sender->bonus)
        return;
    ball->sender->bonus = this;
    state = IN_USE;
}

void Bonus::action_on_use() {}

void Bonus::action_on_ball_move(Ball *ball) {}

CRGB Bonus::get_color() {
    return color;
}

void Bonus::display() {
    if(state != DISPLAYED)
        return;
    strip.turn_range_on(get_color(), coord, coord + len);
}

bool Bonus::display_ball(Ball *ball) {
    return false;
}

float Bonus::get_speed_boost() {
    return 0;
}

bool Bonus::auto_send_back(Ball *ball) {
    return false;
}

bool Bonus::can_be_added() {
    return game.ball_count == 1 && state == NONE;
}

SpeedingBonus::SpeedingBonus()
    : Bonus(SPEEDING_BONUS_LENGTH, SPEEDING_BONUS_PROBABILITY, CRGB(255, 0, 255)) {}

void SpeedingBonus::action_on_use() {
    speed_increase = 1;
}

float SpeedingBonus::get_speed_boost() {
    speed_increase += 0.005;
    return speed_increase;
}

RainbowBonus::RainbowBonus() : Bonus(RAINBOW_BONUS_LENGTH, RAINBOW_BONUS_PROBABILITY) {}

void RainbowBonus::action_on_display() {
    Bonus::action_on_display();
    previous_micros = game.current_micros;
}

void RainbowBonus::action_on_use() {
    hsv_color.hue = 0;
}

CRGB RainbowBonus::get_color() {
    while(previous_micros + 1000 < game.current_micros) {
        hsv_color.hue += 1;
        previous_micros += 1000;
    }
    return (CRGB)hsv_color;
}

void RainbowBonus::display() {
    if(state != DISPLAYED)
        return;
    get_color();
    strip.turn_range_rainbow(coord, coord + len, hsv_color.hue, 5);
}

bool RainbowBonus::display_ball(Ball *ball) {
    if(ball->sender->direction > 0) {
        strip.turn_range_rainbow(ball->start_position, ball->position, hsv_color.hue, 5);
    } else {
        strip.turn_range_rainbow(ball->position, ball->start_position,
                                 hsv_color.hue - 5 * (ball->start_position - ball->position), 5);
    }
    return true;
}

bool RainbowBonus::auto_send_back(Ball *ball) {
    ball->speed_boost = ball->perfect_hit_boost;
    return true;
}

BlinkingBonus::BlinkingBonus()
    : Bonus(BLINKING_BONUS_LENGTH, BLINKING_BONUS_PROBABILITY, CRGB(128, 128, 0)) {}

void BlinkingBonus::action_on_take(Ball *ball) {
    if(ball->bonus)
        return;
    ball->bonus = this;
    state = IN_USE;
}

bool BlinkingBonus::check_time() {
    current_time = millis();
    // blink is not even, the ball is shown a bit more than it is hidden
    uint16_t actual_delay = show ? blinking_delay * 1.5 : blinking_delay;
    if(current_time < previous_time + actual_delay)
        return false;
    previous_time = current_time;
    return true;
}

CRGB BlinkingBonus::get_color() {
    if(check_time())
        show = !show;

    if(show)
        return Bonus::get_color();
    else
        return CRGB::Black;
}

BreakoutBonus::BreakoutBonus()
    : Bonus(BREAKOUT_BONUS_LENGTH, BREAKOUT_BONUS_PROBABILITY, CRGB(128, 128, 128)) {}

void BreakoutBonus::action_on_display() {
    show = true;
    previous_micros = game.current_micros;
}

bool BreakoutBonus::check_time() {
    // blink is not even, the ball is shown a bit more than it is hidden
    uint16_t actual_delay = show ? blinking_delay * 1.5 : blinking_delay;
    if(game.current_micros < previous_micros + actual_delay)
        return false;
    previous_micros = game.current_micros;
    return true;
}

CRGB BreakoutBonus::get_color() {
    if(state == IN_USE)
        return Bonus::get_color();

    if(check_time())
        show = !show;

    if(show)
        return Bonus::get_color();
    else
        return CRGB::Black;
}

void BreakoutBonus::display() {
    if(state == NONE)
        return;
    strip.turn_range_on(get_color(), coord, coord + len);
}

void BreakoutBonus::action_on_take(Ball *ball) {
    state = IN_USE;
}

void BreakoutBonus::action_on_ball_move(Ball *ball) {
    if(state != IN_USE)
        return;
    if(ball->sender->direction == 1 && ball->position != coord - 1)
        return;
    if(ball->sender->direction == -1 && ball->position != coord + len + 1)
        return;
    state = NONE;
    ball->switch_player();
}

LifeAddBonus::LifeAddBonus() : Bonus(LIFE_ADD_BONUS_LENGTH, LIFE_ADD_BONUS_PROBABILITY, CRGB(0, 255, 0)) {}

void LifeAddBonus::action_on_take(Ball *ball) {
    ball->sender->life_points++;
    if(ball->bonus) {
        state = NONE;
    } else {
        ball->bonus = this;
        state = IN_USE;
    }
}

bool LifeAddBonus::can_be_added() {
    return state == NONE;
}

BallAddBonus::BallAddBonus() : Bonus(BALL_ADD_BONUS_LENGTH, BALL_ADD_BONUS_PROBABILITY) {}

void BallAddBonus::action_on_take(Ball *ball) {
    game.add_ball(ball->sender);
    state = NONE;
}

float BallAddBonus::get_color_transition_increment(float pixel) {
    if(pixel <= 128)
        return pixel / 128 + 0.4;
    else
        return -1 * pixel / 255 + 1.4;
}

CRGB BallAddBonus::get_color() {
    // breathing effect from red to blue and backward
    float increment = get_color_transition_increment(red);

    if(go_towards_blue) {
        red -= increment;
        blue += increment;
    } else {
        red += increment;
        blue -= increment;
    }

    if(red <= 0 || blue >= 255) {
        go_towards_blue = false;
        red = 0;
        blue = 255;
    } else if(red >= 255 || blue <= 0) {
        go_towards_blue = true;
        red = 255;
        blue = 0;
    }

    return CRGB(red, 0, blue);
}

bool BallAddBonus::can_be_added() {
    return game.ball_count < game.ball_total && state == NONE;
}
