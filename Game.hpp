#ifndef GAME_H
#define GAME_H

#include "Animation.hpp"
#include "Ball.hpp"
#include "Bonus.hpp"
#include "LedStrip.hpp"
#include "Player.hpp"
#include "config.h"
#include <Arduino.h>
#include <FastLED.h>

class Player;
class Strip;
class Ball;
class Bonus;

extern const LifeAddBonus life_add_bonus;
extern const SpeedingBonus speeding_bonus;
extern const RainbowBonus rainbow_bonus;
extern const BlinkingBonus blinking_bonus;
extern const BreakoutBonus breakout_bonus;
extern const BallAddBonus ball_add_bonus;

enum GameState {
    WAITING = 0,
    GETTING_READY,
    PLAYING,
    ENDING,
};

class Game {
  private:
    Player *sender;
    Player *receiver;
    Bonus *scheduled_bonus;
    Ball balls[3] = {Ball(), Ball(), Ball()};
    unsigned long autoplay_timer = 0;
    void pingpong();
    void switch_player();
    void display_ball();
    void display_hitting_zone_dot();
    void display_bonus();
    void maybe_schedule_bonus();
    void add_scheduled_bonus(Ball *ball);
    void check_for_bonus(Ball *ball);
    bool check_buttons();

  public:
    Game();
    GameState state = WAITING;
    static const unsigned long speed = DELAY;
    static const short bonuses_count = 6;
    Bonus *bonuses[bonuses_count] = {
        &life_add_bonus, &speeding_bonus, &rainbow_bonus, &blinking_bonus, &breakout_bonus, &ball_add_bonus,
    };
    Player players[2] = {
        Player(BUTTON_NEAR_PIN, CRGB(255, 0, 0), HITTING_RANGE, POINTS_TO_WIN),
        Player(BUTTON_FAR_PIN, CRGB(0, 0, 255), HITTING_RANGE, POINTS_TO_WIN),
    };
    static const unsigned long ball_total = 3;
    uint8_t ball_count = 0;
    unsigned long current_micros;
    void play();
    bool should_start();
    void init();
    void start_new_round(Player *sender, bool first_round = false);
    void end_game(Player *player);
    Ball *add_ball(Player *player);
    void remove_ball(Ball *ball);
    void send_back(Ball *ball);
};

#endif
