# Pongui

Faire PONG avec une GUIrlande : parce que c'est notre projet !

## Formattage automatique du code

Il est nécessaire d'utiliser le pre-commit hook fourni pour contribuer au projet.

Après installation de pre-commit, il suffit de lancer la commande `pre-commit install` à la racine du projet pour que chaque nouveau commit soit formatté automatiquement.

## Personnalisation des paramètres du jeu

Vitesse de la balle, probabilité d'apparition des bonus, comme on a pas tous le même talent ces différents paramètres sont des constantes modifiables.

Elles sont définies dans le fichier `default_config.h`, mais plutôt que d'aller les y modifier il est conseillé de créer un fichier `config_override.h` et d'y redéfinir les paramètres voulus.

## Documentation

Parce que la flemme de faire plein de commits pour avoir un bon README, voici
l'indémodable pad : https://pad.inpt.fr/p/pongui.

### Contributions

Ce programme est placé sous la license libre GPLv3, qui s'applique également à toutes les contributions.
