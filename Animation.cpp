#include "Animation.hpp"
#include "jeu.hpp"

void Animation::init() {
    this->start_time = millis();
}

bool Animation::done() {
    if((duration_ms && millis() - start_time > duration_ms) || is_done) {
        strip.turn_all_off();
        return true;
    }
    return false;
}

bool Animation::check_time(int ms_delay) {
    current_time = millis();
    if(current_time - previous_time < ms_delay)
        return false;
    previous_time = current_time;
    return true;
}

RandomColors::RandomColors() {
    this->duration_ms = 60000;
}

void RandomColors::play() {
    uint16_t i = random(strip.num_leds);
    strip.set_random_color_led(i);
}

void GameAnimation::build_score_bar() {
    len_score_bar = 0;
    for(uint8_t i = 0; i < 3; i++)
        score_bar[len_score_bar++] = CHSV(96, 255, 255 / (3 - i));

    uint8_t score_length = max(game.players[0].initial_life_points, game.players[0].life_points);
    for(uint8_t i = 0; i < score_length; i++)
        if(i < game.players[0].life_points)
            score_bar[len_score_bar++] = game.players[0].color;
        else
            score_bar[len_score_bar++] = CRGB::Black;

    score_bar[len_score_bar++] = CRGB::Green;

    score_length = max(game.players[1].initial_life_points, game.players[1].life_points);
    for(uint8_t i = 0; i < score_length; i++)
        if(i >= (score_length - game.players[1].life_points))
            score_bar[len_score_bar++] = game.players[1].color;
        else
            score_bar[len_score_bar++] = CRGB::Black;

    for(uint8_t i = 1; i < 4; i++)
        score_bar[len_score_bar++] = CHSV(96, 255, 255 / i);
}

void GameAnimation::display_score_bar() {
    uint16_t middle = strip.num_leds / 2;
    uint16_t start = middle - ((len_score_bar + 1) / 2);
    for(uint16_t i = start; i < start + len_score_bar; i++)
        strip.turn_on(score_bar[i - start], i);
}

WinAnimation::WinAnimation(Player *player) {
    this->player = player;
    this->duration_ms = 5000;
    this->start = 0;
    GameAnimation::build_score_bar();
}

RoundAnimation::RoundAnimation(Player *player, bool first_round) {
    this->player = player;
    this->duration_ms = !first_round ? 3500 : 2000;
    this->show = false;
    GameAnimation::build_score_bar();
}

void WinAnimation::play() {
    if(!check_time(80))
        return;
    strip.turn_range_off(start, strip.num_leds - 1, spacing);
    start = (start - player->direction + spacing) % spacing;
    strip.turn_range_on(player->color, start, strip.num_leds - 1, spacing);
    GameAnimation::display_score_bar();
}

void RoundAnimation::play() {
    uint16_t start = 0;
    uint16_t end = strip.num_leds - 1;
    uint16_t middle = strip.num_leds / 2;
    if(player->direction > 0)
        end = middle + 1;
    else
        start = middle;
    if(!check_time(600 / (max(player->initial_life_points - player->life_points, 1))))
        return;
    if(show)
        strip.turn_all_off();
    else
        strip.turn_range_on(player->color, start, end);
    show = !show;
    GameAnimation::display_score_bar();
}

void AccumulatingDots::init() {
    speed = initial_speed;
    position = strip.num_leds / 2;
    last_dot_position = strip.num_leds;
    dot_color = strip.get_random_color();
}

void AccumulatingDots::play() {
    if(!check_time(speed))
        return;

    strip.turn_off(position - 1);
    strip.turn_off(strip.num_leds - position);

    strip.turn_on(dot_color, position);
    strip.turn_on(dot_color, strip.num_leds - position - 1);

    if(speed > 10)
        speed -= 1;

    if(++position >= last_dot_position || position >= strip.num_leds) {
        last_dot_position = position - 1;
        position = strip.num_leds / 2;
        speed = round(initial_speed - (strip.num_leds / 2 - position) / 3);
        dot_color = strip.get_random_color();
    }

    if(last_dot_position == strip.num_leds / 2)
        is_done = true;
}
