#ifndef PLAYER_H
#define PLAYER_H

#include <Arduino.h>
#include <FastLED.h>

class Bonus;
class Ball;

class Player {
  private:
    byte button_pin;
    byte button_state;
    bool check_for_pressing();

  public:
    CRGB color;
    uint16_t position;
    uint16_t first_hit_position;
    byte hitting_range;
    int8_t direction;
    byte initial_life_points;
    byte life_points;
    bool is_a_bot = false;
    Bonus *bonus;
    Player(byte pin, CRGB color, byte hitting_range, byte life_points);
    Player *opponent;
    void check_button_press();
    bool pressing = false;
    void init();
    bool holding();
    bool send_back(Ball *ball);
    void score();
    CRGB get_dot_color();
};

#endif
