#include "Player.hpp"
#include "Ball.hpp"
#include "Bonus.hpp"

Player::Player(byte button_pin, CRGB color, byte hitting_range, byte initial_life_points) {
    this->button_pin = button_pin;
    this->color = color;
    this->hitting_range = hitting_range;
    this->initial_life_points = initial_life_points;
    this->init();
}

void Player::init() {
    this->life_points = initial_life_points;
}

bool Player::holding() {
    return digitalRead(button_pin) == LOW;
}

void Player::check_button_press() {
    pressing = check_for_pressing();
}

bool Player::check_for_pressing() {
    if(is_a_bot)
        return true;

    byte value = digitalRead(button_pin);

    // Ensure longpressing accounts only for one press
    if(button_state == value)
        return false;

    button_state = value;
    return value == LOW;
}

bool Player::send_back(Ball *ball) {
    uint16_t min_position = direction == 1 ? position : first_hit_position;
    uint16_t max_position = direction == 1 ? first_hit_position : position;
    if(!ball->is_in_range(min_position, max_position)) {
        if(pressing && !is_a_bot &&
           ball->is_in_range(min_position - hitting_range, max_position + hitting_range))
            ball->disable_send_back = true;
        return false;
    }

    if(bonus && bonus->auto_send_back(ball))
        return true;

    if(ball->disable_send_back)
        return false;

    if(!pressing)
        return false;

    if(is_a_bot)
        ball->position = random(min_position, max_position + 1);

    if(ball->position == first_hit_position)
        ball->speed_boost = ball->first_hit_boost;
    else if(ball->position == position)
        ball->speed_boost = ball->perfect_hit_boost;
    else if(direction == 1 && ball->position < 3)
        ball->speed_boost = ball->late_hit_boost;
    else if(direction == -1 && ball->position > position - 3)
        ball->speed_boost = ball->late_hit_boost;
    else
        ball->speed_boost = 1;

    return true;
}

CRGB Player::get_dot_color() {
    return bonus ? bonus->get_color() : color;
}
