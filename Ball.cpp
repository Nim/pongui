#include "Ball.hpp"
#include "Bonus.hpp"
#include "LedStrip.hpp"
#include "Player.hpp"
#include "jeu.hpp"

bool Ball::is_in_range(short start, short end) {
    return position >= start && position <= end;
}

void Ball::display() {
    if(bonus && bonus->display_ball(this))
        return;

    CRGB color = bonus ? bonus->get_color() : sender->color;
    if(speed_boost == perfect_hit_boost) {
        if(sender->direction > 0) {
            strip.turn_range_on(color, start_position, position);
        } else {
            strip.turn_range_on(color, position, start_position);
        }
    } else {
        for(short i = 0; i < length; i++) {
            short p = position - i * sender->direction;
            if(p < 0 || p > strip.num_leds - 1)
                break;
            strip.turn_on(color.fadeToBlackBy(255 * i / (length - 1)), p);
        }
    }
}

Ball *Ball::activate(Player *player) {
    active = true;
    disable_send_back = false;
    speed_boost = 1;
    previous_micros = micros();
    sender = player;
    receiver = player->opponent;
    start_position = sender->position;
    position = start_position;
    return this;
};

void Ball::deactivate() {
    active = false;
    remove_bonus();
};

void Ball::use_bonus() {
    bonus = sender->bonus;
    bonus->action_on_use();
    sender->bonus = nullptr;
}

void Ball::remove_bonus() {
    bonus->state = NONE;
    bonus = nullptr;
}

void Ball::switch_player() {
    Player *temp = sender;
    sender = receiver;
    receiver = temp;
    start_position = position;
}

float Ball::get_speed_boost() {
    float bonus_speed_boost = bonus ? bonus->get_speed_boost() : 0;
    return speed_boost + bonus_speed_boost;
}

Player *Ball::move() {
    unsigned long speed = game.speed / get_speed_boost();

    while(previous_micros + speed < game.current_micros) {
        previous_micros += speed;
        position += sender->direction;
        if(!is_in_range(0, strip.num_leds - 1)) {
            return receiver;
        }
        for(uint8_t i = 0; i < game.bonuses_count; i++)
            game.bonuses[i]->action_on_ball_move(this);
    }

    return nullptr;
}
