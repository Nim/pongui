#ifndef ANIMATION_H
#define ANIMATION_H

#include "Game.hpp"
#include "LedStrip.hpp"
#include "Player.hpp"
#include <Arduino.h>
#include <FastLED.h>

class Game;
class Player;
class Strip;

class Animation {
  protected:
    unsigned int duration_ms = 0; // 0 means infinite
    unsigned long current_time;
    unsigned long previous_time;
    bool is_done = false;
    bool check_time(int ms_delay);

  public:
    unsigned long start_time;
    bool can_be_deleted = true;
    virtual void init();
    virtual void play() = 0;
    bool done();
};

class RandomColors : public Animation {
  public:
    RandomColors();
    void play() override;
};

class GameAnimation : public Animation {
  protected:
    CRGB score_bar[60];
    uint8_t len_score_bar;

  public:
    void display_score_bar();
    void build_score_bar();
};

class WinAnimation : public GameAnimation {
  protected:
    byte start;
    byte spacing = 4;
    Player *player;

  public:
    WinAnimation(Player *player);
    void play() override;
};

class RoundAnimation : public GameAnimation {
  protected:
    Player *player;
    bool show;

  public:
    RoundAnimation(Player *player, bool first_round = false);
    void play() override;
};

class AccumulatingDots : public Animation {
  protected:
    uint16_t initial_speed = 80;
    uint16_t speed;
    CRGB dot_color;
    uint16_t position;
    uint16_t last_dot_position;

  public:
    void init() override;
    void play() override;
};

#endif
