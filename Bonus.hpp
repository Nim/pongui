#ifndef BONUS_H
#define BONUS_H

#include <Arduino.h>
#include <FastLED.h>

class Game;
class Player;
class Ball;
class Strip;

enum State {
    NONE,
    DISPLAYED, // bonus is displayed on strip, no one has taken it yet
    IN_USE,    // player has taken the bonus, or it is currently in effect
};

class Bonus {
  protected:
    CRGB color;

  public:
    uint32_t probability;
    State state = NONE;
    uint16_t coord;
    uint16_t display_position_trigger;
    short len;
    Bonus(short len, uint32_t probability, CRGB color = 0);
    virtual void action_on_display();
    virtual bool can_be_added();
    virtual void action_on_take(Ball *ball);
    virtual void action_on_use();
    virtual void action_on_ball_move(Ball *ball);
    virtual CRGB get_color();
    virtual void display();
    virtual bool display_ball(Ball *ball);
    virtual float get_speed_boost();
    virtual bool auto_send_back(Ball *ball);
};

class SpeedingBonus : public Bonus {
  protected:
    float speed_increase;

  public:
    SpeedingBonus();
    float get_speed_boost() override;
    void action_on_use() override;
};

class RainbowBonus : public Bonus {
  protected:
    unsigned long previous_micros;
    CHSV hsv_color = CHSV(0, 240, 255);

  public:
    RainbowBonus();
    void action_on_display() override;
    void action_on_use() override;
    void display() override;
    CRGB get_color() override;
    bool display_ball(Ball *ball) override;
    bool auto_send_back(Ball *ball) override;
};

class BlinkingBonus : public Bonus {
  protected:
    bool show = true;
    unsigned int blinking_delay = 150;
    unsigned long current_time;
    unsigned long previous_time = 0;
    bool check_time();

  public:
    BlinkingBonus();
    CRGB get_color() override;
    void action_on_take(Ball *ball) override;
};

class BreakoutBonus : public Bonus {
  private:
    bool show = true;
    unsigned long blinking_delay = 300000;
    unsigned long current_time;
    unsigned long previous_micros = 0;
    bool check_time();

  public:
    BreakoutBonus();
    CRGB get_color() override;
    void display() override;
    void action_on_display() override;
    void action_on_take(Ball *ball) override;
    void action_on_ball_move(Ball *ball) override;
};

class LifeAddBonus : public Bonus {
  public:
    LifeAddBonus();
    void action_on_take(Ball *ball) override;
    bool can_be_added() override;
};

class BallAddBonus : public Bonus {
  private:
    bool go_towards_blue = true;
    float red = 255;
    float blue = 0;
    float get_color_transition_increment(float pixel);

  public:
    BallAddBonus();
    CRGB get_color() override;
    bool can_be_added() override;
    void action_on_take(Ball *ball) override;
};

#endif
