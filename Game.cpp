#include "Game.hpp"
#include "jeu.hpp"

const LifeAddBonus life_add_bonus = LifeAddBonus();
const SpeedingBonus speeding_bonus = SpeedingBonus();
const RainbowBonus rainbow_bonus = RainbowBonus();
const BlinkingBonus blinking_bonus = BlinkingBonus();
const BreakoutBonus breakout_bonus = BreakoutBonus();
const BallAddBonus ball_add_bonus = BallAddBonus();

Game::Game() {
    players[0].position = 0;
    players[0].first_hit_position = HITTING_RANGE - 1;
    players[0].direction = 1;
    players[0].opponent = &players[1];
    players[1].position = NUM_LEDS - START_OFFSET - END_OFFSET - 1;
    players[1].first_hit_position = players[1].position - HITTING_RANGE + 1;
    players[1].direction = -1;
    players[1].opponent = &players[0];
}

void Game::init() {
    randomSeed(millis());
    autoplay_timer = 0;
    players[0].init();
    players[1].init();
    start_new_round(sender, true);
}

bool Game::should_start() {
    if(!check_buttons())
        // User released the button before starting autoplay mode
        return autoplay_timer != 0;
    if(!autoplay_timer)
        autoplay_timer = millis();
    else if(millis() - autoplay_timer > 4000) {
        receiver->is_a_bot = true;
        return true;
    }
    return false;
}

bool Game::check_buttons() {
    for(byte i = 0; i < 2; i++) {
        if(players[i].holding()) {
            sender = &players[i];
            receiver = &players[(i + 1) % 2];
            return true;
        }
    }
    return false;
}

void Game::play() {
    switch(state) {
    case GETTING_READY:
        if(!strip.animate()) {
            add_ball(sender);
            state = PLAYING;
        }
        break;
    case PLAYING:
        pingpong();
        break;
    case ENDING:
        if(!strip.animate())
            state = WAITING;
        break;
    default:
        return;
    }
}

void Game::pingpong() {
    current_micros = micros();

    strip.turn_all_off();

    for(uint8_t i = 0; i < 2; i++) {
        players[i].check_button_press();
    }

    for(uint8_t i = 0; i < ball_total; i++) {
        Ball *ball = &balls[i];
        if(!ball->active)
            continue;

        ball->display();

        if(ball->sender->pressing)
            check_for_bonus(ball);

        if(ball->receiver->send_back(ball)) {
            send_back(ball);
            maybe_schedule_bonus();
        }

        Player *hitted_player = ball->move();
        if(hitted_player) {
            remove_ball(ball);
            if(--hitted_player->life_points == 0)
                return end_game(hitted_player->opponent);
            else if(ball_count == 0)
                return start_new_round(hitted_player->opponent);
        }

        add_scheduled_bonus(ball);
    }

    display_bonus();
    display_hitting_zone_dot();
}

void Game::send_back(Ball *ball) {
    if(ball->bonus)
        ball->remove_bonus();

    ball->switch_player();

    if(ball->sender->bonus)
        ball->use_bonus();

    if(ball->speed_boost == ball->first_hit_boost) { // sneaky attack
        ball->start_position = round(strip.num_leds / 2);
        ball->position = ball->start_position;
    }
}

void Game::remove_ball(Ball *ball) {
    ball->deactivate();
    ball_count--;
}

void Game::start_new_round(Player *sender, bool first_round) {
    for(short i = 0; i < bonuses_count; i++)
        bonuses[i]->state = NONE;
    maybe_schedule_bonus();

    for(short i = 0; i < 2; i++)
        players[i].bonus = nullptr;

    this->sender = sender;
    strip.turn_all_off();
    strip.set_animation(new RoundAnimation(sender, first_round));
    state = GETTING_READY;
}

Ball *Game::add_ball(Player *player) {
    for(uint8_t i = 0; i < ball_total; i++) {
        if(!balls[i].active) {
            ball_count++;
            return balls[i].activate(player);
        }
    }
    return nullptr;
}

void Game::end_game(Player *winner) {
    players[0].is_a_bot = players[1].is_a_bot = false;
    for(uint8_t i = 0; i < ball_total; i++)
        balls[i].deactivate();
    ball_count = 0;
    strip.turn_all_off();
    strip.set_animation(new WinAnimation(winner));
    state = ENDING;
}

void Game::display_hitting_zone_dot() {
    for(byte i = 0; i < 2; i++) {
        strip.turn_on(players[i].get_dot_color(), players[i].first_hit_position);
#if SHOW_LAST_HIT_POSITION
        strip.turn_on(players[i].get_dot_color(), players[i].position);
#endif
    }
}

void Game::display_bonus() {
    for(short i = 0; i < bonuses_count; i++)
        bonuses[i]->display();
}

void Game::maybe_schedule_bonus() {
    for(short i = 0; i < bonuses_count; i++) {
        Bonus *bonus = bonuses[i];
        if(!bonus->can_be_added())
            continue;
        if(random(bonus->probability + 1) == bonus->probability) {
            // compute random position and random apparition time during ball travel
            bonus->coord = random(players[0].hitting_range * 2,
                                  strip.num_leds - bonus->len - players[1].hitting_range * 2);
            bonus->display_position_trigger =
                random(players[0].hitting_range, strip.num_leds - players[1].hitting_range);
            scheduled_bonus = bonus;
        }
    }
}

void Game::add_scheduled_bonus(Ball *ball) {
    if(scheduled_bonus && scheduled_bonus->display_position_trigger == ball->position) {
        scheduled_bonus->state = DISPLAYED;
        scheduled_bonus->action_on_display();
        scheduled_bonus = nullptr;
    }
}

void Game::check_for_bonus(Ball *ball) {
    for(short i = 0; i < bonuses_count; i++) {
        Bonus *bonus = bonuses[i];
        if(bonus->state == DISPLAYED && ball->is_in_range(bonus->coord, bonus->coord + bonus->len)) {
            bonus->action_on_take(ball);
        }
    }
}
