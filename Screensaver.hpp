#ifndef SCREENSAVER_H
#define SCREENSAVER_H

#include "Animation.hpp"
#include "LedStrip.hpp"

class Screensaver {
  public:
    void start();
    void animate();
};

#endif
